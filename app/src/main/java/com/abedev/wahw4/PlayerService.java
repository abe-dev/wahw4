package com.abedev.wahw4;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by home on 01.09.2016.
 */
public class PlayerService extends Service {
    public static final String PACKAGE_NAME = PlayerService.class.getPackage().getName();
    public static final int EMPTY_SLIDE_ID = -1;
    public static final String EXTRA_SLIDE_ID = "slideId";
    public static final String EXTRA_SLIDE_URI = "slideUri";
    public static final String BROADCAST_ACTION = "com.abedev.wahw4.PlayerService.ShowSlide";
    private static final String TAG = PlayerService.class.getSimpleName();
    public static final int AUTO_PLAY_DELAY = 5000;
    private int currentSlideId = EMPTY_SLIDE_ID;
    private final IBinder binder = new PlayerBinder();
    private static String[] slides = new String[] {
            "slide_1",
            "slide_2",
            "slide_3",
            "slide_4",
            "slide_5"
    };
    private Handler mainHandler = new Handler(Looper.getMainLooper());
    private Runnable playNextSlide = new PlayNextSlide(AUTO_PLAY_DELAY);
    private boolean isAutoPlay = false;
    private Handler autoPlayHandler = null;
    private HandlerThread autoPlayThread = null;

    private boolean isDestroyed;


    @Override
    public void onCreate() {
        Log.d(TAG,String.format("onCreate Thread %d",Thread.currentThread().getId()));
        super.onCreate();
        isDestroyed = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,String.format("onBind Thread %d",Thread.currentThread().getId()));
        currentSlideId = intent.getIntExtra(EXTRA_SLIDE_ID,EMPTY_SLIDE_ID);
        return binder;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG,String.format("onDestroy Thread %d",Thread.currentThread().getId()));
        if (isAutoPlay) {
            stopPlaying();
        }
        isDestroyed = true;
        super.onDestroy();
    }

    public void showNext() {
        goToNext();
        showSlide();

    }

    public void showCurrent() {
        if (currentSlideId!=EMPTY_SLIDE_ID)
            showSlide();
    }

    private void goToNext() {
        currentSlideId++;
        if (currentSlideId >= slides.length)
            currentSlideId = 0;
    }

    public void showPrevious() {
        goToPrevious();
        showSlide();

    }

    private void goToPrevious() {
        currentSlideId--;
        if (currentSlideId < 0)
            currentSlideId = slides.length-1;

    }
    public void play() {
        if (isAutoPlay)
            stopPlaying();
        else
            startPlaying();
    }

    private void startPlaying() {
        autoPlayThread = new HandlerThread("autoPlay");
        autoPlayThread.start();
        autoPlayHandler = new Handler(autoPlayThread.getLooper());
        autoPlayHandler.post(playNextSlide);
        isAutoPlay = true;
    }

    private void stopPlaying() {
        isAutoPlay = false;
        autoPlayHandler.removeCallbacks(playNextSlide);
        autoPlayThread.quitSafely();
    }



    private void showSlide() {
        new Thread(new LoadSlide(currentSlideId))
                .start();
    }


    public class PlayerBinder extends Binder {
        public PlayerService getService() {
            return PlayerService.this;
        }
    }

    private class DeliverSlide implements Runnable {
        private final Uri uri;

        public DeliverSlide(Uri uri) {
            this.uri = uri;
        }

        @Override
        public void run() {
            Intent intent = new Intent(BROADCAST_ACTION);
            intent.putExtra(EXTRA_SLIDE_ID,currentSlideId);
            intent.putExtra(EXTRA_SLIDE_URI,uri);
            LocalBroadcastManager.getInstance(PlayerService.this)
                    .sendBroadcast(intent);
        }
    }

    private class PlayNextSlide implements Runnable {
        final int delay;
        public PlayNextSlide(int delay) {
            this.delay = delay;
        }
        @Override
        public void run() {
            Log.d(TAG,String.format("PlayNextSlide Thread %d",Thread.currentThread().getId()));

            if (isAutoPlay) {
                goToNext();
                autoPlayHandler.postDelayed(playNextSlide, delay);
                new LoadSlide(currentSlideId).run();
            }
            else {
                Log.d(TAG,String.format("PlayNextSlide (auto play disabled) Thread %d",Thread.currentThread().getId()));
            }
        }
    }

    private class LoadSlide implements Runnable {
        private final int slideId;
        public LoadSlide(int slideId) {
            this.slideId = slideId;
        }
        @Override
        public void run() {
            Log.d(TAG,String.format("LoadSlide Thread %d",Thread.currentThread().getId()));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.d(TAG,"LoadSlide interrupted!");
                return;
            }
            Uri uri = Uri.parse(String.format("android.resource://%1$s/raw/%2$s",PACKAGE_NAME,slides[slideId]));
            if (!isDestroyed)
                mainHandler.post(new DeliverSlide(uri));

        }
    }
}
