package com.abedev.wahw4;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView textSlideUri;
    private ImageView imgSlide;
    private int slideId = PlayerService.EMPTY_SLIDE_ID;
    private PlayerService playerService;
    private ServiceConnection playerConnection = new PlayerServiceConnection();
    private boolean isBound = false;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textSlideUri = (TextView) findViewById(R.id.text_slide_uri);
        imgSlide = (ImageView) findViewById(R.id.img_slide);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, String.format("LocalBroadcastManager  onReceive Thread %d", Thread.currentThread().getId()));
                showSlide(intent);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,new IntentFilter(PlayerService.BROADCAST_ACTION));
        Intent intent = new Intent(this,PlayerService.class);
        intent.putExtra(PlayerService.EXTRA_SLIDE_ID,slideId);
        this.bindService(intent,playerConnection,Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isBound) {
            unbindService(playerConnection);
            isBound = false;
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    private void showSlide(final Intent intent) {
        Log.d(TAG, String.format("Show slide on Thread %d", Thread.currentThread().getId()));
        slideId = intent.getIntExtra(PlayerService.EXTRA_SLIDE_ID, PlayerService.EMPTY_SLIDE_ID);
        final Uri uri = intent.getParcelableExtra(PlayerService.EXTRA_SLIDE_URI);
        textSlideUri.setText(uri.toString());
        new DrawableWorkerTask(imgSlide).execute(uri);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_previous:
                if (isBound)
                    playerService.showPrevious();
                break;
            case R.id.btn_play:
                if (isBound)
                    playerService.play();
                break;
            case R.id.btn_next:
                if (isBound)
                    playerService.showNext();
                break;

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PlayerService.EXTRA_SLIDE_ID,slideId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        slideId = savedInstanceState.getInt(PlayerService.EXTRA_SLIDE_ID, PlayerService.EMPTY_SLIDE_ID);
    }

    private class PlayerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            playerService = ((PlayerService.PlayerBinder) service).getService();
            isBound = true;
            playerService.showCurrent();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    }

    class DrawableWorkerTask extends AsyncTask<Uri, Void, Drawable> {
        private final WeakReference<ImageView> imageViewReference;

        public DrawableWorkerTask(ImageView imageView) {
            imageViewReference = new WeakReference<>(imageView);
        }

        @Override
        protected Drawable doInBackground(Uri... params) {
            Uri uri = params[0];
            InputStream stream;
            try {
                stream = getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                Log.d(TAG,"Error loading drawable!",e);
                return null;
            }
            return Drawable.createFromStream(stream,uri.toString());
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            if (drawable != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageDrawable(drawable);
                }
            }
        }
    }
}
